# eventhunter

Microservices:
* eventhunter-authentication:
  https://gitlab.com/williamgates/eventhunter-authentication
* eventhunter-eventmanager:
  https://gitlab.com/williamgates/eventhunter-eventmanager
* eventhunter-frontend:
  https://gitlab.com/williamgates/eventhunter-frontend

Site:
https://event-hunter.herokuapp.com/
